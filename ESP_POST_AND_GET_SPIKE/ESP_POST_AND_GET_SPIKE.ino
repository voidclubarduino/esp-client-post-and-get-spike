/**
 * POST AND GET SPIKE FROM ESP8266
 *
 *  Created by Bruno Horta
 *  ON: 31 May 2016
 *
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
ESP8266WiFiMulti WiFiMulti;

void setup() {
    Serial.begin(115200);
    WiFiMulti.addAP("SID", "password");
    Serial.flush();
}

void loop() {
  /**
   * GET
   * */
    // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) {
        HTTPClient http;
        Serial.print("[HTTP] begin...\n");
        // configure traged server and url
        http.begin("http://192.168.1.136:8083/api/arduino"); //HTTP
        Serial.print("[HTTP] GET...\n");
        // start connection and send HTTP header
        int httpCode = http.GET();
        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            Serial.printf("[HTTP] GET... code: %d\n", httpCode);
            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                Serial.println(payload);
            }
        } else {
            Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }
        http.end();
    }

    delay(2000);
   /**
   * POST
   * */
    // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) {
        HTTPClient http; 
        Serial.print("[HTTP] begin...\n");
        // configure traged server and url
        http.begin("http://192.168.1.136:8083/api/arduino"); //HTTP
        //HEADERS: example Content-Type Json
        http.addHeader("Content-Type","application/json");
        Serial.print("[HTTP] POST...\n");
        // start connection and send HTTP header
   
        int httpCode = http.POST("{\"myObject\":nice}");
        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            Serial.printf("[HTTP] POST... code: %d\n", httpCode);
            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                Serial.println(payload);
            }
        } else {
            Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }
        http.end();
    }

    delay(10000);
}

